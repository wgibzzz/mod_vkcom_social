<?php
/**
 * Template file.
 *
 * @package  Module
 * @author   Pavel Poronko
 * @link     http://gibzzz.tk
 * @since    0.2
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div class="vk-widget">
    <?php if (isset($response)): ?>
        <div class="info">
            <a href="<?php echo $response['info']['link'] ?>">
                <img src="<?php echo $response['info']['photo']; ?>"
                     alt="<?php echo $response['info']['name']; ?>"
                     class="entity-pic"/>
                <span><?php echo $response['info']['name']; ?></span>
            </a>
        </div>
        <div class="nano">
            <?php if (isset($response['wall']['entries'])): ?>
                <div class="entry-list content">
                    <?php
                    $entries = $response['wall']['entries'];
                    $creators = $response['wall']['creators'];
                    ?>
                    <?php foreach ($entries as $entry): ?>
                        <div class="entry entry-<?php echo $entry['id']; ?>">
                            <div class="entry-info">
                                <?php
                                if (isset($creators[$entry['from_id']]['name'])) {
                                    $name = $creators[$entry['from_id']]['name'];
                                } else {
                                    $name = $creators[$entry['from_id']]['first_name'] . ' ' . $creators[$entry['from_id']]['last_name'];
                                }
                                ?>
                                <a class="entry-author-img"
                                   href="<?php echo $creators[$entry['from_id']]['link'] ?>">
                                    <img src="<?php echo $creators[$entry['from_id']]['photo']; ?>" alt=""/>
                                </a>
                                <a class="entry-author-name"
                                   href="<?php echo $creators[$entry['from_id']]['link'] ?>">
                                    <?php echo $name; ?>
                                </a>

                                <?php if ($entry['post_type'] == 'copy'): ?>
                                    <?php
                                    if (isset($creators[$entry['copy_owner_id']]['name'])) {
                                        $originName = $creators[$entry['copy_owner_id']]['name'];
                                    } else {
                                        $originName = $creators[$entry['copy_owner_id']]['first_name'] . ' ' . $creators[$entry['copy_owner_id']]['last_name'];
                                    }
                                    ?>
                                    <a class="entry-origin-info"
                                       href="<?php echo $creators[$entry['copy_owner_id']]['link']; ?>">
                                        <img class="entry-origin-photo"
                                             src="<?php echo $creators[$entry['copy_owner_id']]['photo']; ?>"
                                             alt=""
                                             width="25"/>
                                        <i class="icon-retweet"></i>
                                        <?php echo $originName; ?>
                                    </a>
                                <?php endif; ?>
                                <div class="clearfix"></div>
                            </div>
                            <div class="entry-content">
                                <?php echo $entry['text']; ?>
                                <div class="clearfix"></div>
                                <?php if (isset($entry['attachments'])): ?>
                                    <?php foreach ($entry['attachments'] as $attach): ?>
                                        <div class="entry-attachment">
                                            <?php
                                            $attachmentString = $attach['type'] . '-' . $attach['date'] . $attach['created'];
                                            ?>
                                            <?php if (isset($attach['src'])): ?>
                                                <a href="#<?php echo $attachmentString; ?>" role="button"
                                                   data-toggle="modal">
                                                    <img src="<?php echo $attach['src_big']; ?>" alt=""/>
                                                </a>
                                            <?php endif; ?>
                                            <?php if (isset($attach['image'])): ?>
                                                <a href="#<?php echo $attachmentString; ?>" role="button"
                                                   data-toggle="modal">
                                                    <img src="<?php echo $attach['image_big']; ?>" alt=""/>
                                                </a>
                                            <?php endif; ?>
                                            <div id="<?php echo $attachmentString; ?>" class="modal hide fade">
                                                <?php if ($attach['title']): ?>
                                                    <div class="modal-header">
                                                        <h3> <?php echo $attach['title']; ?></h3>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="modal-body">
                                                    <?php if (isset($attach['src_xxxbig'])): ?>
                                                        <img src="<?php echo $attach['src_xxxbig']; ?>" alt=""/>
                                                    <?php elseif (isset($attach['src_big'])): ?>
                                                        <img src="<?php echo $attach['src_big']; ?>" alt=""/>
                                                    <?php endif; ?>
                                                    <?php if (isset($attach['image_xbig'])): ?>
                                                        <img src="<?php echo $attach['image_xbig']; ?>" alt=""/>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button"
                                                            class="close"
                                                            data-dismiss="modal"
                                                            aria-hidden="true">&times;</button>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <div class="clearfix"></div>
</div>
<?php if (isset($response['wall'])): ?>
    <?php /* foreach ($response['wall']['wall'] as $entry): ?>
        <?php if (is_array($entry)): ?>
            <div class="entry">
                <?php
                if (isset($response['wall']['creators'][$entry['from_id']]['name'])) {
                    $name = $response['wall']['creators'][$entry['from_id']]['name'];
                } else {
                    $name = $response['wall']['creators'][$entry['from_id']]['first_name'] . " " . $response['wall']['creators'][$entry['from_id']]['last_name'];
                }
                ?>
                <p>
                    <a href="https://vk.com/<?php echo $response['wall']['creators'][$entry['from_id']]['screen_name']; ?>"
                       style="margin-right: 5px; float: left;">
                        <img src="<?php echo $response['wall']['creators'][$entry['from_id']]['photo']; ?>"
                             alt="<?php echo $name; ?>"/>
                    </a>
                    <a href="https://vk.com/<?php echo $response['wall']['creators'][$entry['from_id']]['screen_name']; ?>"
                       style="line-height: 25px;">
                        <?php echo $name; ?>
                    </a>
                </p>
                <?php echo preg_replace('/((http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?)/', '<a href="\1">\1</a>', $entry['text']); ?>
                <br/>
                <?php if ($entry['attachments']): ?>
                    <?php if (isset($entry['attachments'][0][$entry['attachments'][0]['type']]['src_big'])): ?>
                        <img src="<?php echo $entry['attachments'][0][$entry['attachments'][0]['type']]['src_big']; ?>"
                             alt=""/>
                    <?php endif; ?>
                    <?php if (isset($entry['attachments'][0][$entry['attachments'][0]['type']]['image_src'])): ?>
                        <img
                            src="<?php echo $entry['attachments'][0][$entry['attachments'][0]['type']]['image_src']; ?>"
                            alt=""/>
                    <?php endif; ?>
                    <?php if (isset($entry['attachments'][0][$entry['attachments'][0]['type']]['image_big'])): ?>
                        <img
                            src="<?php echo $entry['attachments'][0][$entry['attachments'][0]['type']]['image_big']; ?>"
                            alt=""/>
                    <?php endif; ?>
                    <?php ?>
                    <?php ?>
                    <?php ?>
                    <!--                <pre>--><?php //print_r($entry['attachments']); ?><!--</pre>-->
                <?php endif; ?>
                <br/>
                <br/>
            <span class="badge">
                <i class="icon-comment icon-white"></i> <?php echo $entry['comments']['count']; ?>
            </span>&nbsp;
            <span class="badge badge-important">
                <i class="icon-heart icon-white"></i> <?php echo $entry['likes']['count']; ?>
            </span>&nbsp;
            <span class="badge badge-info">
                <i class="icon-share-alt icon-white"></i> <?php echo $entry['reposts']['count']; ?>
            </span>
                <?php if ($entry['signer_id']): ?>
                    <?php
                    if (isset($response['wall']['creators'][$entry['signer_id']]['name'])) {
                        $signer_name = $response['wall']['creators'][$entry['signer_id']]['name'];
                    } else {
                        $signer_name = $response['wall']['creators'][$entry['signer_id']]['first_name'] . " " . $response['wall']['creators'][$entry['signer_id']]['last_name'];
                    }
                    ?>&nbsp;
                    <a href="https://vk.com/<?php echo $response['wall']['creators'][$entry['signer_id']]['screen_name'] ?>">
                        <i class="icon-user"></i> <?php echo $signer_name; ?>
                    </a>
                <?php endif; ?>&nbsp;
                <i class="icon-time"></i>
                <?php echo date('d M Y в H:i', $entry['date']); ?>
                <br/>
                <br/>
                <div class="clearfix"></div>
            </div>
        <?php endif; ?>
    <?php endforeach; */
    ?>
<?php endif; ?>
<?php if (isset($response['subs'])): ?>
    <div class="well well-small">
        <?php foreach ($response['subs']['users'] as $sub): ?>
            <div class="span1" style="text-align: center;">
                <a href="https://vk.com/<?php echo $sub['screen_name']; ?>"
                   style="color: #222;">
                    <img src="<?php echo $sub['photo_50']; ?>" alt=""/>
                    <br/>
                    <?php echo $sub['first_name']; ?>
                    <?php echo $sub['last_name']; ?>
                </a>
            </div>
        <?php endforeach; ?>
        <div class="clearfix"></div>
    </div>
<?php endif; ?>

<div class="vk-debug">
    <?php jdbg::p($response); ?>
</div>