<?php
/**
 * VK.com Social Module Entry Point
 *
 * @package        Gibz.Modules
 * @subpackage     VK
 * @license        GNU/GPL v2
 * mod_vkcom_social is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

// Include the syndicate functions only once
require_once(dirname(__FILE__) . DS . 'helper.php');


$document = JFactory::getDocument();

$document->addStyleSheet('media/mod_vkcom_social/css/default.css?v1');
$document->addStyleSheet('media/mod_vkcom_social/css/nanoscroller.css?v1');
$document->addScript('media/mod_vkcom_social/js/jquery.nanoscroller.min.js');
$document->addScript('media/mod_vkcom_social/js/script.js');
$document->addScript('media/mod_vkcom_social/js/bootstrap.min.js');

$res = array();
/**
 * Config class init
 */
$res['VKConfig'] = new VKConfig();
$res['VKConfig']->setApiId($params->get('api_id', ''));
$res['VKConfig']->setSecret($params->get('secret', ''));
$res['VKConfig']->setEntityId($params->get('entity_id', ''));
$res['VKConfig']->setEntityType($params->get('entity_type', ''));
$res['VKConfig']->setEntityView($params->get('entity_view', 'wall'));
$res['VKConfig']->setShowAttach($params->get('show_attach', 0));
$res['VKConfig']->setShowVideo($params->get('show_video', 0));


$response = modVKCOMSocialHelper::getVKWidget($res);
$debug    = modVKCOMSocialHelper::getDebug($res);
require(JModuleHelper::getLayoutPath('mod_vkcom_social'));