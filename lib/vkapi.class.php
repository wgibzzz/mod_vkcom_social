<?php
/**
 * Small library file. Provider of VK Api functions in PHP.
 *
 * @see vkapi
 * @package Library
 */

/**
 * VKAPI class for vk.com social network.
 * Created by Oleg Illarionov. Modified and documented by Pavel Poronko.
 *
 * @package  server API methods
 * @link     http://vk.com/developers.php
 * @link     http://gibzzz.tk
 * @author   Oleg Illarionov
 * @modified Pavel Poronko
 * @version  1.0
 */
class vkapi {
    /**
     * Vital parameter for access to VK Api. Required.
     *
     * @var string
     */
    var $api_secret;
    /**
     * Your app id. Required.
     *
     * @var int
     */
    var $app_id;
    /**
     * @var string API entry point.
     */
    var $api_url;

    /**
     * Function, which defines settings of API
     *
     * @param int    $app_id
     * @param string $api_secret
     * @param string $api_url
     */
    function vkapi($app_id, $api_secret, $api_url = 'api.vk.com//api.php') {
        $this->app_id     = $app_id;
        $this->api_secret = $api_secret;
        if (!strstr($api_url, 'http://')) $api_url = 'http://' . $api_url;
        $this->api_url = $api_url;
    }

    /**
     * Entry function of each API request
     *
     * @param string $method Method name - what you need to fetch
     * @param array  $params Array of parameters, such as limit, user\group ids, etc.
     *
     * @return array Array with response
     */
    function api($method, $params = null) {
        if (!$params) $params = array();
        $params['api_id']    = $this->app_id;
        $params['v']         = '3.0';
        $params['method']    = $method;
        $params['timestamp'] = time();
        $params['format']    = 'json';
        $params['random']    = rand(0, 10000);
        ksort($params);
        $sig = '';
        foreach ($params as $k => $v) {
            $sig .= $k . '=' . $v;
        }
        $sig .= $this->api_secret;
        $params['sig'] = md5($sig);
        $query         = $this->api_url . '?' . $this->params($params);
        $res           = file_get_contents($query);

        return json_decode($res, true);
    }

    /**
     * Generates URI string with parameters
     *
     * @param array $params Api secret, app id, etc.
     *
     * @return string Formed URI string for usage in api request function
     */
    function params($params) {
        $pice = array();
        foreach ($params as $k => $v) {
            $pice[] = $k . '=' . urlencode($v);
        }

        return implode('&', $pice);
    }
}
