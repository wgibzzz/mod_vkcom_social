<?php
/**
 * Helper classes file.
 *
 * @package  Module
 * @author   Pavel Poronko
 * @link     http://gibzzz.tk
 * @since    0.1
 */

require_once(dirname(__FILE__) . DS . 'lib' . DS . 'vkapi.class.php');

/**
 * Class modVKCOMSocialHelper
 *
 * @package  Module
 * @author   Pavel Poronko
 * @link     http://gibzzz.tk
 * @version  0.5
 */
class modVKCOMSocialHelper extends JController
{
    /**
     * @var vkapi $VK An protected object, who providing access to VK Api.
     */
    private static $VK;
    /**
     * @var string $VKHost Base href for links to user profiles, groups, etc.
     */
    private static $VKHost = 'https://vk.com/';

    /**
     *
     */
    function display()
    {
        parent::display(true); //true asks for caching.
    }

    /**
     * Retrieves the array of records, entity info, etc
     *
     * @param array $params An object containing the module parameters
     *
     * @access public
     *
     * @return string
     */
    public function getVKWidget($params)
    {
        /**
         * Initial request to VK Api.
         *
         * @var $VK
         */
        self::$VK = new vkapi($params['VKConfig']->getApiId(), $params['VKConfig']->getSecret());

        $method = $params['VKConfig']->getEntityType();
        $entityId = $params['VKConfig']->getEntityId();
        $view = $params['VKConfig']->getEntityView();
        $media = $params['VKConfig']->getShowAttach();
        if ($method == "user") {
            $result = self::getUser($entityId, $view, $media);
        } else {
            $result = self::getGroup($entityId, $view, $media);
        }

        return $result;
    }

    /**
     * Gets an single or multiple user(s), based by $user_ids. Returns an array with user profiles.
     *
     * @param string $user_ids string with multiple or single user ids
     * @param string $view String, which helps to determine next steps - to get wall or to get followers
     * @param null $media Not yet implemented
     *
     * @return array
     */
    private function getUser($user_ids, $view, $media = null)
    {
        $user = array();
        $user['info_raw'] = self::$VK->api("users.get", array(
            "user_ids" => $user_ids,
            "fields" => "photo_50,screen_name"
        ));
        $user['info'] = $user['info_raw']['response'];
        $user['info']['link'] = self::$VKHost . $user['info']['screen_name'];
        if ($view == "subs") {
            $user['subs'] = self::getSubscribers($user_ids, $view, null);
        } else if ($view == "wall") {
            $user['wall'] = self::getWall($user_ids, $media, "user");
        }

        return (array)$user;
    }

    /**
     * Returns an array with subscribers
     *
     * @TODO limit of records (non-urgent)
     *
     * @param string $entity_ids String with entity id(s)
     * @param string $entity_type What you want to fetch: group or user
     * @param int $limit Limit of returned records
     *
     * @return array
     */
    private function getSubscribers($entity_ids, $entity_type, $limit)
    {
        if ($entity_type == "user") {
            $formattedSubs['users'] = self::$VK->api("users.getFollowers", array(
                "user_id" => $entity_ids,
                "fields" => "photo_50, screen_name",
                "count" => 9
            ));


        } else {
            $subscribers = self::$VK->api("groups.getMembers", array(
                "group_id" => $entity_ids,
                "count" => 9
            ));

            $subIds = array();
            foreach ($subscribers['response']['users'] as $user) {
                $subIds[] = $user;
            }

            $subs = implode(',', $subIds);
            $formattedSubs['debug'] = $subscribers['response']['users'];
            $formattedSubs['users_raw'] = self::getUser($subs, null);
            $formattedSubs['users'] = $formattedSubs['users_raw']['info'];
        }


        return (array)$formattedSubs;
    }

    /**
     * Gets, beautifies and returns array with wall records.
     *
     * @TODO limit of records (non-urgent)
     *
     * @param string $entity_ids
     * @param        $media
     * @param        $method
     *
     * @return array
     */
    private function getWall($entity_ids, $media, $method)
    {
        if ($method == "group") {
            $id = "-" . $entity_ids;
        } else {
            $id = $entity_ids;
        }
        $wall_raw = self::$VK->api("wall.get", array(
            "owner_id" => $id,
            "count" => 12,
            "filter" => "all",
            "extended" => 1
        ));
        $wall = $wall_raw['response'];
        $wall['entries'] = array();
        foreach ($wall['wall'] as $entry) {
            if (is_array($entry)) {
                if (isset($entry['attachment']) || isset($entry['attachments'])) {
                    $attachments = self::getAttachments($entry['attachments']);
                    $entry['attachments'] = $attachments;
                }
                $wall['entries'][] = $entry;
            }
        }
        unset($wall['wall']);

        $wall['creators'] = array();
        foreach ($wall['profiles'] as $user) {
            $wall['creators'][$user['uid']] = $user;
            $wall['creators'][$user['uid']]['link'] = self::$VKHost . $user['screen_name'];
        }
        unset($wall['profiles']);
        foreach ($wall['groups'] as $group) {
            $wall['creators']['-' . $group['gid']] = $group;
            $wall['creators']['-' . $group['gid']]['link'] = self::$VKHost . $group['screen_name'];
        }
        unset($wall['groups']);


        return (array)$wall;
    }

    /**
     * Simplifies dat attachment/attachments array for easy navigation
     *
     * @TODO     implement that
     *
     * @param $entry Array with post data
     *
     * @return array $attach
     */
    private function getAttachments($entry)
    {
        $attach = array();
        foreach ($entry as $attachment) {
            $type = $attachment['type'];
            $entry = $attachment[$type];
            $entry['type'] = $type;
            $vid_arr[] = $entry['attachment']['video']['vid'];
            $attach[] = $entry;
        }
        if (isset($entry['attachment'])) {
        }
        unset($entry['attachment']);
        unset($entry['attachments']);

        return $attach;
    }

    /**
     * Returns an array with group info.
     * If wall view requested - returns a non-assoc array with wall records.
     * If group view requested - returns a non-assoc array with profiles.
     *
     * @param $group_ids
     * @param $view
     * @param $media
     *
     * @return array
     */
    private function getGroup($group_ids, $view, $media)
    {
        $group = array();
        $group['info_raw'] = self::$VK->api("groups.getById", array(
            "group_ids" => (string)$group_ids
        ));
        $group['info'] = $group['info_raw']['response'][0];
        $group['info']['link'] = self::$VKHost . $group['info']['screen_name'];

        if ($view == "subs") {
            $group['subs'] = self::getSubscribers($group_ids, $view, null);
        } else {
            $group['wall'] = self::getWall($group_ids, $media, "group");
        }

        return (array)$group;
    }

    /**
     * Debug output for developers.
     *
     * @param $params
     *
     * @return array
     */
    public function getDebug($params)
    {
        /** @var $VK */
        self::$VK = new vkapi($params['VKConfig']->getApiId(), $params['VKConfig']->getSecret());

        $method = $params['VKConfig']->getEntityType();
        $entityId = $params['VKConfig']->getEntityId();
        $view = $params['VKConfig']->getEntityView();
        $media = array(
            "attach" => $params['VKConfig']->getShowAttach(),
            "video" => $params['VKConfig']->getShowVideo()
        );
        if ($method == "user") {
            $result = self::getUser($entityId, $view, $media);
        } else {
            $result = self::getGroup($entityId, $view, $media);
        }

        return (array)$result;
    }

    /**
     * Not yet implemented
     *
     * @TODO implement that
     */
    private function getVideos($owner_id, $video_ids)
    {
    }
}

/**
 * Class VKConfig
 */
class VKConfig
{
    /**
     * Your app id. Required.
     *
     * @var int $api_id
     */
    public $api_id;
    /**
     * Vital parameter for access to VK Api. Required.
     *
     * @var string $secret
     */
    public $secret;
    /**
     * Entity type - such as user or group
     *
     * @var string $entity_type
     */
    public $entity_type;
    /**
     * Your entity id
     *
     * @var int $entity_id
     */
    public $entity_id;
    /**
     * What you want to fetch - wall or followers
     *
     * @var string $entity_view
     */
    public $entity_view;
    /**
     * Not yet implemented
     *
     * @var int $show_video
     */
    public $show_video;
    /**
     * Not yet implemented
     *
     * @var int $show_attach
     */
    public $show_attach;

    /**
     * Getter
     *
     * @return string
     */
    public function getEntityView()
    {
        return $this->entity_view;
    }

    /**
     * Setter
     *
     * @param string $entity_view
     */
    public function setEntityView($entity_view)
    {
        $this->entity_view = $entity_view;
    }

    /**
     * Getter
     *
     * @return int
     */
    public function getShowAttach()
    {
        return $this->show_attach;
    }

    /**
     * Setter
     *
     * @param int $show_attach
     */
    public function setShowAttach($show_attach)
    {
        $this->show_attach = $show_attach;
    }

    /**
     * Getter
     *
     * @return int
     */
    public function getShowVideo()
    {
        return $this->show_video;
    }

    /**
     * Setter
     *
     * @param int $show_video
     */
    public function setShowVideo($show_video)
    {
        $this->show_video = $show_video;
    }

    /**
     * Getter
     *
     * @return int
     */
    public function getApiId()
    {
        return $this->api_id;
    }

    /**
     * Setter
     *
     * @param int $api_id
     */
    public function setApiId($api_id)
    {
        $this->api_id = $api_id;
    }

    /**
     * Getter
     *
     * @return int
     */
    public function getEntityId()
    {
        return $this->entity_id;
    }

    /**
     * Setter
     *
     * @param int $entity_id
     */
    public function setEntityId($entity_id)
    {
        $this->entity_id = $entity_id;
    }

    /**
     * Getter
     *
     * @return string
     */
    public function getEntityType()
    {
        return $this->entity_type;
    }

    /**
     * Setter
     *
     * @param string $entity_type
     */
    public function setEntityType($entity_type)
    {
        $this->entity_type = $entity_type;
    }

    /**
     * Getter
     *
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * Setter
     *
     * @param string $secret
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
    }
}